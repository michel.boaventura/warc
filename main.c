#include "main.h"

int
main(int argc, char *argv[]) {
  warc_t *warc;

  if(argc != 5 && argc != 4)
    handle_error("Usage: ./warc THRESHOLD CONTENT_JSON WARCFILE OUT_FILE_DIR");

  if(argc == 4)
    warc = warc_new(atoi(argv[1]), atoi(argv[2]), argv[3], NULL);
  else
    warc = warc_new(atoi(argv[1]), atoi(argv[2]), argv[3], argv[4]);

  while(!warc_next_response(warc)) {}

  warc_free(warc);

  return 0;
}
