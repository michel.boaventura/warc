#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "errors.h"

#define BUFFSIZE 50000000
#define MIN(a,b) a > b ? b : a

float *relative_bitmap(int *bitmap, int s, int ratio){
  float *relative;
  int agg = s / ratio, i, k = 0;

  relative = calloc(sizeof(float), ratio);

  for(i = 0; i < s; i++){
    k = MIN(i / agg, agg - 1);
    relative[k] += bitmap[i] / (float) agg;
  }
  return relative;
}

size_t
open_file(char *fname, char **buf) {
  int fd;
  struct stat fs;

	fd = open(fname, O_RDONLY);

	if (fd == -1)
		handle_error("open");

  if (fstat(fd, &fs) == -1)
    handle_error("fstat");

	*buf = mmap(NULL, fs.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

  if (*buf == MAP_FAILED)
    handle_error("mmap");

  close(fd);

  return fs.st_size;
}

int main(int argc, char **argv){
  (void) argc;
  char temp, *bitmap;
  int *agg_bitmap, i = 0, j = 0, max, line_max = 0, ratio = 100;
  float *agg;

  agg_bitmap = calloc(sizeof(int), BUFFSIZE);
  bitmap = calloc(sizeof(char), BUFFSIZE);

  max = open_file(argv[1], &bitmap);

  while(i < max){
    temp = bitmap[i];
    if(temp == '\n'){
      if(j > line_max){
        line_max = j;
      }
      j = 0;
    }
    else{
      agg_bitmap[j] += temp - '0';
      j++;
    }
    i++;
  }

  agg = relative_bitmap(agg_bitmap, line_max, ratio);

  for(i = 0; i < ratio; i++){
    printf("%.2f ", agg[i]);
  }
  printf("\n");

  free(agg_bitmap);
  munmap(bitmap, max);
}


