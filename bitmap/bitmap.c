#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void bitmap(char *file1, char *file2){
  FILE *f1 = fopen(file1, "r");
  FILE *f2 = fopen(file2, "r");
  int i = 0, max = 0, f1_size, f2_size;
  char *arq1;
  char *arq2;

  fseek(f1, 0, SEEK_END);
  fseek(f2, 0, SEEK_END);

  f1_size = ftell(f1);
  f2_size = ftell(f2);

  arq1 = malloc(sizeof(char) * f1_size);
  arq2 = malloc(sizeof(char) * f2_size);

  rewind(f1);
  rewind(f2);

  max = f1_size > f2_size ? f1_size : f2_size;

 fread(arq1, sizeof(char), f1_size, f1);
 fread(arq2, sizeof(char), f2_size, f2);


  for(i = 0; i < max; i++){
    if(i > f1_size || i >f2_size){
      printf("%d", 1);
    }
    else{
      printf("%d", arq1[i] == arq2[i] ? 0 : 1);
    }
  }
  printf("\n");
}

int main(int argc, char *argv[]){
  (void) argc;
  bitmap(argv[1], argv[2]);
}
