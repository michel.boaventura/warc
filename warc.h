#ifndef _WARC_H_
#define _WARC_H_

#define _GNU_SOURCE
#define BUFFSIZE 70000000

#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <json-c/json.h>
#include "errors.h"
#include "gzip.h"

typedef enum { warcinfo, response, request, metadata } record_type_t;

typedef struct warc_t {
  record_type_t type;
  size_t buffer_size;
  char *filename, *buffer, *tmpbuffer, *token, *out_dir;
  FILE *file;
  int response;
  int threshold;
  int content_json;
} warc_t;

warc_t *warc_new(int threshold, int content_json, char *file, char *out_dir);
void warc_free(warc_t *warc);
int warc_next_response(warc_t *warc);
void add_key_value_to_json(json_object *jobj, char *token, char *buffer);
void parse_header_first_line(warc_t *warc);
void parse_header(warc_t *warc, json_object *jobj);

#endif
