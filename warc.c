#include "warc.h"

static int
warc_parse_version(warc_t *warc) {
  char header[] = "WARC/1.0";

  if(!fgets(warc->buffer, warc->buffer_size, warc->file))
    return -1;

  warc->buffer[strcspn(warc->buffer, "\r\n")] = '\0';

  if(strcmp(warc->buffer, header))
    handle_error("Expecting '%s', got '%s'", header, warc->buffer);

  return 0;
}

static void
warc_parse_token(warc_t *warc) {
  if(!fgets(warc->tmpbuffer, BUFFSIZE, warc->file))
    handle_error("Error creating buffer");

  if(sscanf(warc->tmpbuffer, "%[^:]: %s\n\n", warc->token, warc->buffer) != 2)
    handle_error("Error on token %s\n", warc->token);

  // seta a flag de response
  if(!strcmp(warc->token, "WARC-Type") && !strcmp(warc->buffer, "response")){
    warc->response = 1;
  }
  else if(!strcmp(warc->token, "WARC-Type")){
    warc->response = 0;
  }
  if(!strcmp(warc->token, "Content-Type") && !strcmp(warc->buffer, "text/dns")){
    warc->response = 0;
  }
}


int
warc_next_response(warc_t *warc) {
  char filename[33];
  char dir_file[100];
  FILE *f;
  json_object *jobj;
  int ret, length;

  ret = warc_parse_version(warc);

  if(ret == -1)
    return -1;

  warc_parse_token(warc);
  assert(!strcmp(warc->token, "WARC-Type"));
  // cria objeto json
  jobj = json_object_new_object();

  // enquanto não chegar em Content-Length
  while(strcmp("Content-Length", warc->token)){
    warc_parse_token(warc);
    add_key_value_to_json(jobj, warc->token, warc->buffer);

    if(!warc->content_json && !strcmp("WARC-Target-URI", warc->token)){
      md5(warc->buffer, filename, strlen(warc->buffer));
      add_key_value_to_json(jobj, "md5", filename);
    }
  }
  // chegou em content length

  length = atoi(warc->buffer);
  if(warc->response && length < warc->threshold ){
    // warc->buffer é o content-length
    fseek(warc->file, 2, SEEK_CUR);
    fread(warc->tmpbuffer, sizeof(char), length, warc->file);
    warc->tmpbuffer[length] = '\0';
    fseek(warc->file, 4, SEEK_CUR);

    parse_header(warc, jobj);

    if(warc->content_json){
      add_key_value_to_json(jobj, "content", warc->buffer);
    }
    else{
      strcpy(dir_file, warc->out_dir);
      strcat(dir_file, "/");
      strcat(dir_file, filename);
      f = fopen(dir_file, "w");
      fprintf(f, "%s", warc->buffer);
      fclose(f);
    }
    printf("%s\n", json_object_to_json_string(jobj));
  }
  else{
    fseek(warc->file, length + 6, SEEK_CUR);
  }

  json_object_put(jobj);

  return 0;
}

warc_t *
warc_new(int threshold, int content_json,  char *path, char *out_dir) {
  warc_t *warc;

  check_file(path);

  warc = malloc(sizeof(warc_t));

  warc->filename = strdup(path);
  warc->file = fopen(warc->filename, "r");
  warc->buffer_size = BUFFSIZE;

  warc->buffer    = calloc(sizeof(char), warc->buffer_size);
  warc->tmpbuffer = calloc(sizeof(char), warc->buffer_size);
  warc->token     = calloc(sizeof(char), warc->buffer_size);

  warc->threshold    = threshold;
  warc->content_json = content_json;

  warc->out_dir = out_dir;

  return warc;
}

void
add_key_value_to_json(json_object *jobj, char *token, char *buffer){
  json_object *jbuffer = json_object_new_string(buffer);
  json_object_object_add(jobj, token, jbuffer);
}

void
warc_free(warc_t *warc) {
  fclose(warc->file);
  free(warc->filename);
  free(warc->buffer);
  free(warc->tmpbuffer);
  free(warc->token);
  free(warc);
}

void
parse_header_first_line(warc_t *warc){
  if(sscanf(warc->tmpbuffer, "%s %s", warc->buffer, warc->token) != 2)
    handle_error("Error header first line %s\n", warc->buffer);
}

void
parse_header(warc_t *warc, json_object *jobj){
  char s[2] = "\n";
  char *str;

  parse_header_first_line(warc);

  add_key_value_to_json(jobj, "protocol", warc->buffer);
  add_key_value_to_json(jobj, "code", warc->token);

  str = strtok(warc->tmpbuffer, s);
  str = strtok(NULL, s);
  // while its not null neither  a single \r
  while(str != NULL && strcmp(str, "\r")){
    if(sscanf(str, "%[^:]: %[^\n]\n", warc->token, warc->buffer) == 0)
      handle_error("Error on token %s\n", warc->token);
    add_key_value_to_json(jobj, warc->token, warc->buffer);
    str = strtok(NULL, s);
  }
  // if the remaining is not NULL, its the content
  if(str != NULL){
    str[strlen(str)] = ' ';
    strcpy(warc->buffer, str);

  }
  else{
    strcpy(warc->buffer, "");
  }
}
