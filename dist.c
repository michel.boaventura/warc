#include "dist.h"

size_t
open_file(char *fname, char **buf) {
  int fd;
  struct stat fs;

	fd = open(fname, O_RDONLY);

	if (fd == -1)
		handle_error("open");

  if (fstat(fd, &fs) == -1)
    handle_error("fstat");

	*buf = mmap(NULL, fs.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

  if (*buf == MAP_FAILED)
    handle_error("mmap");

  close(fd);

  return fs.st_size;
}

int
levenshtein(char *s1, char *s2, size_t s1len, size_t s2len) {
  unsigned int x, y, lastdiag, olddiag, *column, out;

  // Return 0 if the strings are the same
  if (s1len == s2len && strcmp(s1, s2) == 0) {
    return 0;
  }

  column = malloc(sizeof(unsigned int) * (s1len + 1));

  for (y = 1; y <= s1len; y++)
    column[y] = y;
  for (x = 1; x <= s2len; x++) {
    column[0] = x;
    for (y = 1, lastdiag = x-1; y <= s1len; y++) {
      olddiag = column[y];
      column[y] = MIN3(column[y] + 1, column[y-1] + 1, lastdiag + (s1[y-1] == s2[x-1] ? 0 : 1));
      lastdiag = olddiag;
    }
  }
  out = column[s1len];
  free(column);

  return(out);
}

int
calculate_dist_fragments(char *file1, char *file2, float begin, float end){
  char *buf1, *buf2, *temp1 = NULL, *temp2 = NULL;
  size_t s1, s2;
  int lev = 0, piece1_lenght, piece2_lenght;
  int begin_pos, end_pos;

  s1 = open_file(file1, &buf1);
  s2 = open_file(file2, &buf2);

  remove_spaces_breaklines(buf1, &temp1, s1);
  remove_spaces_breaklines(buf2, &temp2, s2);

  // originals not needed anymore
  munmap(buf1, s1);
  munmap(buf2, s2);

  s1 = strlen(temp1);
  s2 = strlen(temp2);



  // calculates the positions
  begin_pos = begin * s1;
  end_pos = end * s1;
  piece1_lenght = end_pos - begin_pos;
  buf1 = temp1 + begin_pos;
  temp1[end_pos] = '\0';

  begin_pos = begin * s2;
  end_pos = end * s2;
  piece2_lenght = end_pos - begin_pos;
  buf2 = temp2 + begin_pos;
  temp2[end_pos] = '\0';
  printf("temp1: %s\ntemp2: %s\n", buf1, buf2);
  printf("temp1_len: %d\ntemp2_len: %d\n", piece1_lenght, piece2_lenght);

  // calculates levenshtein
  lev = levenshtein(buf1, buf2, piece1_lenght, piece2_lenght);

  free(temp1);
  free(temp2);

  return lev;
}

void
remove_spaces_breaklines(char *in, char **out, size_t s){
  size_t i;
  char *ptr2;
  char c = ' ', c1 = '\n', c2 = '\r';
  *out = malloc(sizeof(char)*s);
  ptr2 = *out;

  for(i = 0; i < s; i++){
    if(in[i] != c && in[i] != c1 && in[i] != c2){
      *ptr2 = in[i];
      ptr2++;
    }
  }
  *ptr2 = '\0';
}
